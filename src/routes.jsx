"use strict";

import React from 'react'
import { Switch, Route } from 'react-router-dom'

const Routes = () => (
    <main>
      <Switch>
        <Route exact path='/' component={require('./components/homepage')}/>
        <Route path='/whoami' component={require('./components/who/whoami')}/>
        <Route path='/services' component={require('./components/services/services')}/>
        <Route path='/where' component={require('./components/where/whereami')}/>
        <Route path='/blog' component={require('./components/blog/tempblog')} />        
      </Switch>
    </main>
  )

module.exports = Routes;