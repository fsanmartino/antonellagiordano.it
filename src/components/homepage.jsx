"use strict";

import showdown from 'showdown'

import './common/common.css';
import './homepage.css';
import picture from '../images/picture.jpg'

const React = require('react');
var converter = new showdown.Converter();

class Home extends React.Component {
    constructor(){
        super();
        
          this.state = {
            title: "Title not yet set",
            text: "Text not yet set",
            quote: "Quote not yet set"
        };
    }

    componentDidMount() {
        // Title
        fetch('http://cms.antonellagiordano.it/staticcontent/5ae5c85904a118b6ebdb9c9f')
        .then(function(response) { return response.json(); })
        .then(data => {
                this.setState({
                    title: converter.makeHtml(data.Content) 
                });
            });

        // Main content
        fetch('http://cms.antonellagiordano.it/staticcontent/5ae768247b9d7c1755b50ba4')
        .then(function(response) { return response.json(); })
        .then(data => {
                this.setState({
                    text: converter.makeHtml(data.Content) 
                });
            });
    }

    render() {
        function createMarkup(content) {
            return {__html: content};
        }

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="leftContainer col-md-8">
                        <div>
                            <h2 dangerouslySetInnerHTML={createMarkup(this.state.title)}></h2>
                        </div>
                        <div className="whoAmIText" dangerouslySetInnerHTML={createMarkup(this.state.text)}>
                        </div>
                    </div>
                    <div className="rightContainer col-md-3">
                        <img src={picture} className="img-responsive img-rounded anto-pic" alt="Antonella Giordano" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="quote-container spumante-text greenText text-center" dangerouslySetInnerHTML={createMarkup(this.state.quote)}>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = Home;