"use strict";

import './../common/common.css';
import './whoami.css';
import picture from '../../images/antonellagiordano.jpg'

const React = require('react');

class WhoAmI extends React.Component {
    constructor(){
        super();
        
          this.state = {
            titleFormazione: "Title formazione",
            textFormazione: "Text formazione",
            titleMotivazione: "Title motivazione",
            textMotivazione: "Text motivazione",
            titleObiettivi: "Title obiettivi",
            textObiettivi: "Text obiettivi",
            quote: "Quote not yet set"
        };
    }

    componentDidMount() {
        fetch('http://cms.antonellagiordano.it/api/v1/static-content/cjeqyav3z0034vgahpt39eyup')
        .then(function(response) { return response.json(); })
        .then(data => {
                this.setState({
                    titleFormazione: data.htmlTitle.items[0].content,
                    textFormazione: data.text.items[0].content,
                    quote: data.quote.items[0].content
                });
            });

        fetch('http://cms.antonellagiordano.it/api/v1/static-content/cjeqyzslv0059vgah9kjijfkw')
        .then(function(response) { return response.json(); })
        .then(data => {
                this.setState({
                    titleMotivazione: data.htmlTitle.items[0].content,
                    textMotivazione: data.text.items[0].content,
                });
            });

            fetch('http://cms.antonellagiordano.it/api/v1/static-content/cjeqzdxpk006kvgahm8oxnex4')
            .then(function(response) { return response.json(); })
            .then(data => {
                this.setState({
                    titleObiettivi: data.htmlTitle.items[0].content,
                    textObiettivi: data.text.items[0].content,
                });
            });
    }

    render() {
        function createMarkup(content) {
            return {__html: content};
        }

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="leftContainer col-md-8">
                        <h2  dangerouslySetInnerHTML={createMarkup(this.state.titleFormazione)}></h2>
                        <div className="whoAmIText text-justify" dangerouslySetInnerHTML={createMarkup(this.state.textFormazione)}>
                        </div>
                        <h2  dangerouslySetInnerHTML={createMarkup(this.state.titleMotivazione)}></h2>
                        <div className="whoAmIText text-justify" dangerouslySetInnerHTML={createMarkup(this.state.textMotivazione)}>
                        </div>
                        <h2  dangerouslySetInnerHTML={createMarkup(this.state.titleObiettivi)}></h2>
                        <div className="whoAmIText text-justify" dangerouslySetInnerHTML={createMarkup(this.state.textObiettivi)}>
                        </div>
                    </div>
                    <div className="rightContainer col-md-3">
                        <img src={picture} className="img-responsive img-rounded anto-pic" alt="Antonella Giordano" />
                        
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="quote-container spumante-text greenText text-center" dangerouslySetInnerHTML={createMarkup(this.state.quote)}>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = WhoAmI;
