"use strict";

import React from 'react'
import Header from './common/header'
import Footer from './common/footer'
import Routes from './../routes'
import { Link } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component {
    render() {

        return (
            <div>
                <Header/>
                <div className="container main-content">
                    <Routes/>
                </div>
                <Footer 
                    src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fagiordanonutrizionista%2F&tabs=timeline&width=500&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=119939298109070"
                    width="500" height="200" style={{border: 'none'}} scrolling="no" allowTransparency="true"
                    />
            </div>
        );
    }
}

module.exports = App;
