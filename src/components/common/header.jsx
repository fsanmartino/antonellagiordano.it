"use strict";

import DynamicHeader from './dinamic-header';
import Navbar from 'react-bootstrap/lib/Navbar';
import NavItem from 'react-bootstrap/lib/NavItem';
import Nav from 'react-bootstrap/lib/Nav';
import { LinkContainer } from "react-router-bootstrap";
import { Link } from 'react-router-dom';

import './fonts.css';
import './common.css';
import './header.css';

const React = require('react');

class Header extends React.Component {
  render() {
    return (
      <DynamicHeader hasEffect={true} effectDuration={300} useHeadersDifference={true}>
        <div style={{height: "180px", width: "100%", position: "relative", top: 0, zIndex: 1039, textAlign: "center",}}>
          <div className="container">
            <div className="top-menu-container">
              <div className="row">
                <div className="col-md-5 col-sm-5">
                  <Link to="/">
                    <p className="spumante-text top-header-text">Cultura <span className="greenText">in</span> Cucina</p>
                  </Link>
                </div>
                <div className="col-md-5 col-sm-5 pull-right right-padding-ag hidden-xs">
                  <p className="spumante-text top-header-text">Antonella Giordano</p>
                </div>
              </div>
            </div>
          </div>
          <Navbar fluid>
          <div className="container">
            <Navbar.Header>
              <Navbar.Brand>
                <div className="logo-container hidden-xs">
                  <Link to="/">
                    <img src={require('../../images/logo-img.jpg')} className="bigLogoImg" alt="Cultura in cucina logo" />
                  </Link>
                </div>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav>
                <LinkContainer to="/whoami">
                  <NavItem>
                    <span className="text-menu">Chi Sono</span>
                  </NavItem>
                </LinkContainer>
                <LinkContainer to="/services">
                  <NavItem eventKey={2}>
                    <span className="text-menu">Servizi</span>
                  </NavItem>
                </LinkContainer>
              </Nav>
              <Nav pullRight>
                <LinkContainer to="/where">
                  <NavItem eventKey={1}>
                    <span className="text-menu">Dove sono</span>
                  </NavItem>
                </LinkContainer>
                <LinkContainer to="/blog">
                  <NavItem eventKey={2}>
                    <span className="text-menu">Blog</span>
                  </NavItem>
                </LinkContainer>
              </Nav>
            </Navbar.Collapse>
            </div>
          </Navbar>
        </div>
        <div style={{height: "50px", width: "100%", position: "fixed", top: 0, zIndex: 1039, textAlign: "center", }}>
          <Navbar fluid>
            <div className="container">
              <Navbar.Header>
                <Navbar.Brand>
                  <div className="logo-container hidden-xs">
                    <div className="whiteDiv">
                      <p className="spumante-text logo-replace-text">Cultura <span className="greenText">in</span> Cucina</p>
                    </div>
                  </div>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
              <Navbar.Collapse>
              <Nav>
                <LinkContainer to="/whoami">
                  <NavItem className="test">
                    <span className="text-menu">Chi Sono</span>
                  </NavItem>
                </LinkContainer>
                <LinkContainer to="/services">
                  <NavItem eventKey={2}>
                    <span className="text-menu">Servizi</span>
                  </NavItem>
                </LinkContainer>
              </Nav>
              <Nav pullRight>
                <LinkContainer to="/where">
                  <NavItem eventKey={1}>
                    <span className="text-menu">Dove sono</span>
                  </NavItem>
                </LinkContainer>
                <LinkContainer to="/blog">
                  <NavItem eventKey={2}>
                    <span className="text-menu">Blog</span>
                  </NavItem>
                </LinkContainer>
              </Nav>
              </Navbar.Collapse>
            </div>
          </Navbar>
        </div>
      </DynamicHeader>
    );
  }
}

module.exports = Header;
