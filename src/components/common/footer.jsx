"use strict";

import './common.css';
import './footer.css';

const React = require('react');

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            <a className="plain-link" target="_blank" href="http://www.onb.it/faq-biologo-nutrizionista/">
                                <img src={require('../../images/onb.jpg')} className="onb-logo" alt="Ordine nazionale dei biologi" />
                            </a>
                            <p>Sez. A AA_067955</p>
                        </div>
                        <div className="col-md-3">
                            <a className="plain-link" target="_blank" href="http://www.sinu.it/html/cnt/chi-siamo.asp">
                                <img src={require('../../images/SINU.jpg')} className="sinu-logo" alt="Società Italiana Nutrizione Umana" />
                            </a>
                        </div>
                        <div className="col-md-6 text-right">
                            <iframe src={this.props.src} height={this.props.height} width={this.props.width}/>
                        </div> 
                    </div>
                </div>
            </div>
        );
    }
}   

module.exports = Footer;
