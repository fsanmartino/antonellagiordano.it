"use strict";

import './services.css';
import ServiceList from './serviceList';

const React = require('react');

const cmsBaseUrl = 'http://cms.antonellagiordano.it';

class Services extends React.Component {
    constructor(){
        super();
        
        this.state = {
            title: "",
            text: ""
        };

        this.state.services = [];
    }

    componentDidMount() {
        fetch(cmsBaseUrl + '/api/v1/static-content/cjer3pi7k0080vgahzz7nlxe4')
        .then(function(response) { return response.json(); })
        .then(data => {
                this.setState({
                    title: data.htmlTitle.items[0].content,
                    text: data.text.items[0].content,
                });
            });

        fetch(cmsBaseUrl + '/api/v1/services/')
        .then(function(response) { return response.json(); })
        .then(data => {
            this.setState({
                services: data.results
            })   
        });
    }

    render() {
        var createMarkup = (content) => {
            return {__html: content};
        };

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h2 dangerouslySetInnerHTML={createMarkup(this.state.title)}></h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 text-justify" dangerouslySetInnerHTML={createMarkup(this.state.text)}>
                    </div>
                </div>
                <ServiceList services={this.state.services}/>
            </div>
        );
    }
}

module.exports = Services;