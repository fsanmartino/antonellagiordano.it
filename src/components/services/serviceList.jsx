"use strict";

import Panel from 'react-bootstrap/lib/Panel';
import PropTypes from 'prop-types';

import './services.css';

const React = require('react');

const cmsBaseUrl = 'http://cms.antonellagiordano.it';

class ServiceList extends React.Component {
    render() {
        var createMarkup = (content) => {
            return {__html: content};
        };

        var renderService = (service) => {
            var image = service.image.items[0]._pieces[0].item.attachment;
            var imageUrl = cmsBaseUrl + image._urls.original.replace('.jpg', '.one-half.jpg');
            var imageAlt = image.name;
            var text = service.text.items[0].content;

            return (
                <div key={service._id}>
                    <Panel id={service._id} defaultExpanded>
                        <Panel.Heading>
                            <Panel.Title toggle>
                            {service.title}
                            </Panel.Title>
                        </Panel.Heading>
                        <Panel.Collapse>
                            <Panel.Body>
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-sm-2">
                                            <img src={imageUrl} alt={imageAlt} className="img-responsive" />
                                        </div>
                                        <div className="col-sm-8" dangerouslySetInnerHTML={createMarkup(text)}>
                                        </div>
                                    </div>
                                </div>
                            </Panel.Body>
                        </Panel.Collapse>
                    </Panel>
                </div>
            );
        }

        return (
            <div className="container-fluid">
                <div className="services-class-container">
                    {this.props.services.map(renderService, this)}
                </div>
            </div>
        );
    }
}

ServiceList.propTypes = {
    services: PropTypes.array.isRequired
};

module.exports = ServiceList;