"use strict";

import './../common/fonts.css';
import './../common/common.css';
import './wip.css';

const React = require('react');

class Wip extends React.Component {
    render() {
        return (
            <div className="container  main-content">
                
                <div className="logo-container text-center">
                    <img src={require('./../../images/logo-img.jpg')} className="bigLogoImg" alt="Cultura in cucina logo" />
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="quote-container spumante-text greenText text-center">
                            <p>Stiamo lavorando ad una nuova versione del sito web.</p>
                            <p>Tornate presto a trovarci!</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = Wip;
