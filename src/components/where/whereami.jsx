"use strict";

import './../common/common.css';
import './whereami.css';

const React = require('react');

class WhereAmI extends React.Component {
    render() {
        return (
            <div className="container-fluid locationsContainer">
                <div className="row">
                    <div className="col-md-6">
                        <h2>Torino - Libertarea</h2>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2818.2681722301545!2d7.667377415543948!3d45.060070779098105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47886d3e3bb20a91%3A0x3526c1a197739b24!2sVia+Pastrengo%2C+22%2C+10128+Torino+TO!5e0!3m2!1sit!2sit!4v1514732415296" width="100%" height="450" frameBorder="0" allowFullScreen></iframe>
                    </div>
                    <div className="col-md-6">
                        <h2>Torino - Studio SRF</h2>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2819.0294287603797!2d7.636394815724913!3d45.04462466936207!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47886cd2d7d8aaa5%3A0x8371f1ef6966b379!2sVia+Filadelfia%2C+200%2C+10137+Torino+TO!5e0!3m2!1sit!2sit!4v1514736202417" width="100%" height="450" frameBorder="0" allowFullScreen></iframe>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <h2>Poirino - Gym Studio Health Club</h2>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2825.0075455037827!2d7.835636315721903!3d44.92318207740001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4787e278b9f7b501%3A0xec0366fa7a9de426!2sStrada+Vicinale+Savona%2C+2%2C+10046+Poirino+TO!5e0!3m2!1sit!2sit!4v1514736283498" width="100%" height="450" frameBorder="0" allowFullScreen></iframe>
                    </div>
                    <div className="col-md-6">
                        <h2>San Germano Chisone - Farmacia Tron</h2>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2826.1924246620406!2d7.2369171155386445!3d44.89908117909829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47882e4720641e69%3A0xd1c4be87a79cc3fb!2sPiazzetta+dall&#39;Orso%2C+2%2C+10065+San+Germano+Chisone+TO!5e0!3m2!1sit!2sit!4v1514732750657" width="100%" height="450" frameBorder="0" allowFullScreen></iframe>
                    </div>
                </div>
            </div>

        );
    }
}

module.exports = WhereAmI;