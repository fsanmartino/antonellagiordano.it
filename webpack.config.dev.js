// Import Webpack npm module
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  // Which file is the entry point to the application
  entry: [
    path.resolve(__dirname, 'src/index')
  ],
  target: 'web',  
  // Which file types are in our project, and where they are located
  resolve: {
    extensions: ['.js', '.jsx']
  },
  // Where to output the final bundled code to
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    sourceMapFilename: 'bundle.map.js'
  },
  plugins: [
      // Create HTML file that includes reference to bundled JS.
      new HtmlWebpackPlugin({
         favicon: 'src/images/favico.ico',
         template: 'src/index.html',
         inject: true
      })
  ],
  devtool: '#source-map',
  module: {
    // How to process project files with loaders
    loaders: [
      // Process any .js or .jsx file with Babel
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['babel-loader']
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2|ico)$/,
        loader: 'url-loader',
        options: {
          limit: 1000
        }
      },
    ]
  }
}
