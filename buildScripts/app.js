var path = require('path');
var express = require('express');

var port = 80;
var app = express();

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname, '/index.html'));
});

app.listen(port, function(err){
    if (err){
        // eslint-disable-next-line no-console
        console.log(err);
    }
    else{
        // eslint-disable-next-line no-console
        console.log("Node app is running at localhost:" + port); 
    }
 });